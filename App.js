import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Login from './Components/Login.js';
import Signup from './Components/Signup.js';
import OtpVerification from './Components/OtpVerification.js';
import Payment from './Components/Payment.js';
import Success from './Components/Success.js';
import Error from './Components/Error.js';

import { reusableStyles } from './Helpers/CommonStyles.js'


const RootStack = createStackNavigator({
  Home: {
    screen: Login,
    navigationOptions: {
      title: '',
      headerStyle: reusableStyles.redBox
    }
  },
  Signup: {
    screen: Signup,
    navigationOptions: {
      title: 'Signup',
      headerStyle: reusableStyles.redBox
    }
  },
  OtpVerification: {
    screen: OtpVerification,
    navigationOptions: {
      title: 'OTP Verification',
      headerStyle: reusableStyles.redBox
    }
  },
  Payment: {
    screen: Payment,
    navigationOptions: {
      title: 'Payment',
      headerStyle: reusableStyles.redBox
    }
  },
  Success: {
    screen: Success,
    navigationOptions: {
      title: 'Success',
      headerStyle: reusableStyles.redBox
    }
  },
  Error: {
    screen: Error,
    navigationOptions: {
      title: 'Error',
      headerStyle: reusableStyles.redBox
    }
  }
}, {
    cardStyle: {
      shadowColor: 'transparent',
    },
    navigationOptions: {
      header: {
        style: {
          elevation: 0,       //remove shadow on Android
          shadowOpacity: 0,   //remove shadow on iOS
          shadowOffset: {
            height: 0
          },
          shadowRadius: 0,
          borderBottomWidth: 0,
        }
      }
    }
  });
const App = createAppContainer(RootStack);

export default App;
