import React from "react";
import {
  StyleSheet,
} from "react-native";

var txtFontSize = 10

export const reusableStyles = StyleSheet.create({
    loginLogo:{
      width:138, 
      height:138,
      top:-40
    },
    navigationBar:{
      backgroundColor: '#de0101',
        color: '#fff', 
    },
    w80: {
        width: '80%',
        alignSelf: 'center'
      },
      blackMediumTxt: {
        color: "#000",
        fontSize: 14,
        textAlign: 'center',
        fontFamily: 'Montserrat-Medium',
      },
      blackBoldTxt: {
        color: "#000",
        fontSize: 14,
        textAlign: 'center',
        fontFamily: 'Montserrat-Bold',
        marginLeft: 5
      },
      bsBtnGreen: {
        fontSize: 14,
        textAlign: 'center',
        backgroundColor: '#71c341',
        fontFamily: 'Montserrat-Bold',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        color: "#fff",
        alignItems: 'center',
        borderRadius: 5
      },
      ErrorBx: {
        backgroundColor: '#e00101'
      },
      ErrorIcon: {},
      resultBxHeading: {
        color: '#fff',
        fontFamily: 'Montserrat-Medium',
        fontSize: 20,
        marginTop: 10
      },
      resultBxIcon: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 50,
      },
      successBx: {
        backgroundColor: '#56b71e'
      },
      resultBx: {
        flex: 1,
        flexDirection: 'column'
      },
      resultHdr: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
      },
      resultCnt: {
        flex: 3,
        justifyContent: 'space-around'
      },
      paymentBx: {
        alignSelf: 'stretch',
      },
      greySeparator: {
        height: 1,
        width: "100%",
        backgroundColor: '#e0e0e0',
        marginTop: 10,
        marginBottom: 10
      },
      bsDrpDwn: {
        height: 10,
        alignSelf: 'stretch',
        borderRadius: 5,
        color: '#000',
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        borderColor: '#d1d1d1',
        borderWidth: 1,
        padding: 10,
        height: 200,
        textAlignVertical: 'top'
      },
      mT10: {
        margin: 10
      },
      bsHeadingMd: {
        fontSize: 12,
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'Montserrat-SemiBold',
      },
      backArrow: {
        fontSize: 12,
        textAlign: 'left',
        color: '#fff',
        position: 'absolute',
        top: 70,
        left: 20
      },
    
      redBoxSmall: {
        flex: .5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#de0101',
        position: 'relative',
        top: -10
      },
      greySmallTxt: {
        fontSize: 12,
        textAlign: 'center',
        color: 'rgba(154, 154, 154, 0.5)',
        fontFamily: 'Montserrat-Medium',
        marginTop: 5,
        alignSelf: 'flex-start'
      },
      aL: {
        alignSelf: 'flex-start'
      },
      aC: {
        alignSelf: 'center'
      },
      aR: {
        alignSelf: 'flex-end'
      },
    
      w50: {
        width: '50%'
      },
      bsHeading: {
        fontSize: 14,
        textAlign: 'center',
        color: '#000',
        fontFamily: 'Montserrat-SemiBold',
        marginTop: 75,
        marginBottom: 25
      },
      calendarIcon: {
        position: 'absolute',
        bottom: 10,
        right: 10
      },
      profileImg: {
        marginBottom: 25,
        alignItems: 'center',
        position: 'relative'
      },
      profileIcon: {
        position: 'absolute',
        bottom: 2,
        right: 2,
      },
      bsTxtInput: {
        height: 40,
        alignSelf: 'stretch',
        borderRadius: 5,
        color: '#000',
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        borderColor: '#d1d1d1',
        borderWidth: 1,
        padding: 10,
        height: 200,
        textAlignVertical: 'top'
      },
      formScrollContainer: {
        width: '100%'
      },
      stepperLine: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
      },
      lineSeparator: {
        height: 1,
        width: 50,
        backgroundColor: '#e9e9e9',
        top: 15
      },
      stepperActTxt: {
        color: '#fff',
        fontFamily: 'Montserrat-Medium',
      },
      stepperTxt: {
        color: '#000',
        fontFamily: 'Montserrat-Medium',
      },
      stepperActElement: {
        height: 30,
        width: 30,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff0000',
        color: '#fff',
        alignItems: 'center',
        shadowColor: '#ddd',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: 1,
          width: 0
        },
      },
      stepperElement: {
        height: 30,
        width: 30,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        color: '#000',
        alignItems: 'center',
        fontFamily: 'Montserrat-Medium',
        shadowColor: '#ddd',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: 1,
          width: 0
        },
      },
      headerCntr: {
        position: 'relative'
      },
      hr: {
        height: 1,
        width: 50,
        backgroundColor: '#f90000',
        position: 'absolute',
        top: 35,
        left: 10
      },
      buttonWhtTxt: {
        color: '#fff',
        fontFamily: 'Montserrat-Bold',
      },
      bsBtnRed: {
        fontSize: 14,
        textAlign: 'center',
        backgroundColor: '#f90000',
        fontFamily: 'Montserrat-Bold',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        color: "#fff",
        alignItems: 'center',
        borderRadius: 5
      },
      buttonRedTxt: {
        color: '#f90000',
        fontFamily: 'Montserrat-Bold',
      },
      bsBtnWht: {
        fontSize: 14,
        textAlign: 'center',
        color: '#f90000',
        backgroundColor: '#fff',
        fontFamily: 'Montserrat-Bold',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        alignItems: 'center',
        borderRadius: 5,
        shadowColor: '#ddd',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: 1,
          width: 0
        },
      },
      buttonRedTxtSm: {
        fontSize: 12,
        textAlign: 'center',
        color: '#f90000',
        backgroundColor: '#fff',
        fontFamily: 'Montserrat-Bold',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
      },
      elemCntr: {
        width: '100%',
        justifyContent: 'center',
        marginBottom: 25,
        position: 'relative'
      },
      bsInput: {
        height: 40,
        alignSelf: 'stretch',
        borderRadius: 5,
        color: '#000',
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        borderColor: '#d1d1d1',
        borderWidth: 1,
        padding: 10
      },
      container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: '#ddd',
      },
      redBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#de0101',
      },
      whiteBox: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      },
      contentBox: {
        flex: 1,
        paddingTop: 75,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: '#ddd',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: 1,
          width: 0
        },
        marginLeft: 50,
        marginRight: 50,
        top: -75,
        position: 'relative'
      },
      heading: {
        fontSize: 20,
        letterSpacing: 1,
        textAlign: 'center',
        color: '#000',
        fontFamily: 'Montserrat-SemiBold',
        marginBottom: 35
      }
});

//export const commonStyles = {commonStyles}