let instance = null;

export default class NetworkHelper{
	constructor() {
        if(!instance){
              instance = this;
        }

        this.url = "";
        this.method = "";
        this.accessToken = "";
        this.data = "";
        this.accept = "application/json";
        this.contentType = "application/json";
        this.header = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        return instance;
    }

    setUrl(url){
        this.url = url;
    }
    setMethod(method){
        this.method = method;
    }
    setAccessToken(accessToken){
        this.accessToken = accessToken;
    }
    setData(data){
        this.data = data;
    }
    setAccept(accept){
        this.accept = accept
    }
    setContentType(contentType){
        this.contentType = contentType;
    }
    setHeader(header){
        this.header = Object.assign(this.header, header);
    }

    execute(successHandler, errorHandler){
        if(this.method.toLowerCase() == "get"){
          var requestParam = {
              method : this.method,
              headers: this.header
            };
        } else {
          var requestParam = {
              method : this.method,
              headers: this.header,
              body: JSON.stringify(this.data)
            };
         
        }
        console.log('header',JSON.stringify(requestParam));
        fetch(this.url, requestParam)
          .then((response) =>
          {
            var status = response.status.toString();
            console.log('status =>'+status);
   
            switch (status) {
            case '200':
                if(response._bodyText != ""){
                  response.json().then((responseData) =>
                  {
                    successHandler(responseData);
                  });
                } else {
                  successHandler({});
                }
                break;
          	case '204':
          		successHandler({});
                break;
              case '404':
              case '403':
              case '401':
              case '400':
                  errorHandler("Something went wrong.", status);
                  break;
              default:
                  errorHandler("Something went wrong", status);
             }
          }
        )
        .catch(function(err) {
            errorHandler("Something went wrong." + err, 100);
        });
      }
}

