import React from "react";
import config from '../BuildConfig.js';

export function getErrorMessages() {
   return {
     'serverErrorMsg': 'Something went wrong',
     'unAuthorisedUserMsg' : 'You are not authorized to access this feature.',
     'requestCouldNotComplete': 'Request could not complete, please try again',     
   }
 }

export function getRestBaseURL() {
   return {
      'baseUrl': config.baseUrl,
  }
}

var Urls = {
    getClassList: getRestBaseURL().baseUrl+"demo"
}

export { Urls as default };

export function getImageUrls(){
   return {
     'dream11' : 'demo.png',
 }
}