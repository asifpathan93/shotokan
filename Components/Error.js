import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome.js';
import { Text, View, ImageBackground, TouchableHighlight } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'

export default class Error extends React.Component {
    render() {
        return (
            <View style={reusableStyles.resultBx}>
                <ImageBackground source={require('../Images/particles.png')} style={[reusableStyles.resultHdr, reusableStyles.ErrorBx]}>
                    <View style={reusableStyles.resultBxIcon}>
                        <Icon name="check" size={60} color="#ff0000" />
                    </View>
                    <Text style={reusableStyles.resultBxHeading}>Error !</Text>
                </ImageBackground>
                <View style={reusableStyles.resultCnt}>
                    <Text style={reusableStyles.blackMediumTxt}>Thank you for your payment. {"\n"}Your monthly subscription for May is {"\n"} processed successfully.{"\n"} Your Enrollment id is
                <Text style={reusableStyles.blackBoldTxt}> #234q</Text></Text>
                    <View style={reusableStyles.elemCntr}>
                        <TouchableHighlight underlayColor="#f90000" style={[reusableStyles.w80, reusableStyles.bsBtnGreen]} onPress = { ()=> this.props.navigation.push('Home')}>
                            <Text style={reusableStyles.buttonWhtTxt}>GO TO HOME PAGE</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}
