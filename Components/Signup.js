import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome.js';
import { Text, View, ImageBackground, TextInput, TouchableHighlight, ScrollView } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'

export default class Signup extends React.Component {
  render() {
    return (
      <View style={reusableStyles.container}>
        <View style={reusableStyles.whiteBox}>
          <View style={reusableStyles.contentBox}>
            <View style={reusableStyles.stepperLine}>
              <View style={reusableStyles.stepperActElement}>
                <Text style={reusableStyles.stepperActTxt}>1</Text>
              </View>
              <View style={reusableStyles.lineSeparator}></View>
              <View style={reusableStyles.stepperElement}>
                <Text>2</Text>
              </View>
              <View style={reusableStyles.lineSeparator}></View>
              <View style={reusableStyles.stepperElement}>
                <Text>3</Text>
              </View>
            </View>
            <View>
              <Text style={reusableStyles.bsHeading}>Admission Form</Text>
            </View>
            <ScrollView style={reusableStyles.formScrollContainer}>
              <View style={reusableStyles.profileImg}>
                <ImageBackground source={require('../Images/profile.png')} style={{ width: 73, height: 73 }}  >
                  <Icon name="plus" size={20} color="#ff0000" style={reusableStyles.profileIcon} />
                </ImageBackground>
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Email Id" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Mobile Number" onChangeText={(text) => this.setState({ text })} />
                <Text style={[reusableStyles.greySmallTxt, reusableStyles.aL]}>You will recive OTP on this number</Text>
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} secureTextEntry={true} placeholder="Set Password" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} secureTextEntry={true} placeholder="Confirm Password" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Full Name" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput multiline={true} placeholder="Address" numberOfLines={10} style={reusableStyles.bsTxtInput} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Date of Birth" onChangeText={(text) => this.setState({ text })} />
                <Icon name="calendar" size={20} color="#999" style={reusableStyles.calendarIcon} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Father Occupation" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Father Number" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Mother Occupation" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Mother Number" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TextInput style={reusableStyles.bsInput} placeholder="Joining Date" onChangeText={(text) => this.setState({ text })} />
              </View>
              <View style={reusableStyles.elemCntr}>
                <TouchableHighlight underlayColor="#f90000" style={[reusableStyles.bsBtnRed, reusableStyles.w50, reusableStyles.aR]} onPress = { ()=> this.props.navigation.push('OtpVerification')}>
                  <Text style={reusableStyles.buttonWhtTxt}>NEXT</Text>
                </TouchableHighlight>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
