import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  TextInput
} from "react-native";

export  const ButtonComponent = ({buttonAction, buttonText}) =>   

  	<TouchableOpacity onPress={buttonAction} style={{ marginTop: 25, marginHorizontal:0 }}>
        <Text>
            {buttonText}
        </Text>
    </TouchableOpacity>;

