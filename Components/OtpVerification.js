import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome.js';
import { Text, View, TextInput, TouchableHighlight } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'

export default class OtpVerification extends React.Component {
    render() {
        return (
            <View styles={reusableStyles.formContainer}>
                <View>
                    <Text style={reusableStyles.bsHeading}>OTP Verification</Text>
                </View>
                <View>
                    <Text style={[reusableStyles.greySmallTxt, reusableStyles.aC]}>
                        We have sent you an SMS with a OTP code for verification
              </Text>
                    <Text style={[reusableStyles.aC, reusableStyles.mT10]}>
                        +91 9867955974
              </Text>
                    <View style={reusableStyles.elemCntr}>
                        <TextInput style={reusableStyles.bsInput} placeholder="Enter OTP" onChangeText={(text) => this.setState({ text })} />
                    </View>
                    <Text style={[reusableStyles.greySmallTxt, reusableStyles.aC]}>
                        Didn’t recieve the OTP?
              </Text>
                    <View style={reusableStyles.elemCntr}>
                        <TouchableHighlight underlayColor="#fff" style={reusableStyles.bsBtnLnk} >
                            <Text style={reusableStyles.buttonRedTxtSm}>RESEND OTP</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={reusableStyles.elemCntr}>
                        <TouchableHighlight underlayColor="#f90000" style={[reusableStyles.bsBtnWht, reusableStyles.w50, reusableStyles.aL]} onPress = { ()=> this.props.navigation.push('Signup')} >
                            <Text style={reusableStyles.buttonRedTxt}>PREV</Text>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor="#f90000" style={[reusableStyles.bsBtnRed, reusableStyles.w50, reusableStyles.aR]} onPress = { ()=> this.props.navigation.push('Payment')} >
                            <Text style={reusableStyles.buttonWhtTxt}>NEXT</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}
