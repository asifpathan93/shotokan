import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome.js';
import { Text, View, TouchableHighlight, Picker } from 'react-native';
import { reusableStyles } from '../Helpers/CommonStyles.js'

export default class Payment extends React.Component {
    render() {
        return (
            <View styles={reusableStyles.formContainer}>
                <View>
                    <Text style={reusableStyles.bsHeading}>Payment</Text>
                </View>
                <View>
                    <View style={reusableStyles.elemCntr}>
                        <Picker style={reusableStyles.bsDrpDwn} onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}>
                            <Picker.Item label="Choose Plan" value="cp" />
                            <Picker.Item label="Choose a" value="ca" />
                        </Picker>
                    </View>
                    <View style={reusableStyles.elemCntr}>
                        <View style={reusableStyles.paymentBx}>
                            <Text style={[reusableStyles.aL, reusableStyles.greySmallTxt]}>Fees Amount</Text>
                            <Text style={[reusableStyles.aR]}>RS 5000</Text>
                        </View>
                        <View style={reusableStyles.paymentBx}>
                            <Text style={[reusableStyles.aL, reusableStyles.greySmallTxt]}>GST(18%)</Text>
                            <Text style={reusableStyles.aR}>RS 50</Text>
                        </View>
                        <View style={reusableStyles.greySeparator}></View>
                        <View style={reusableStyles.paymentBx}>
                            <Text style={[reusableStyles.aL, reusableStyles.greySmallTxt]}>Total</Text>
                            <Text style={reusableStyles.aR}>RS 5050</Text>
                        </View>
                    </View>
                    <View style={reusableStyles.elemCntr}>
                        <TouchableHighlight underlayColor="#f90000" style={[reusableStyles.bsBtnWht, reusableStyles.w50, reusableStyles.aL]} onPress = { ()=> this.props.navigation.push('OtpVerification')} >
                            <Text style={reusableStyles.buttonRedTxt}>PREV</Text>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor="#f90000" style={[reusableStyles.bsBtnRed, reusableStyles.w50, reusableStyles.aR]} onPress = { ()=> this.props.navigation.push('Success')} >
                            <Text style={reusableStyles.buttonWhtTxt}>PAY</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>

        );
    }
}
