import React, { Component } from 'react';
import {Text, View, Image, TextInput, TouchableHighlight } from 'react-native';
import {reusableStyles} from '../Helpers/CommonStyles.js'

export default class Login extends React.Component{

  render() {
    return (
      <View style={reusableStyles.container}>
        <View style={reusableStyles.redBox}>
          <Image source={require('../Images/shotokan-logo.png')}  style={reusableStyles.loginLogo}  />
        </View>
        <View style={reusableStyles.whiteBox}>
          <View style={reusableStyles.contentBox}> 
            <View style={reusableStyles.headerCntr}>
              <Text style={reusableStyles.heading}>LOGIN</Text>
              <View style={reusableStyles.hr}></View>  
            </View>  
            <View style={reusableStyles.elemCntr}>
              <TextInput style={reusableStyles.bsInput} placeholder="Email" onChangeText={(text) => this.setState({text})} />
            </View>
            <View style={reusableStyles.elemCntr}>
              <TextInput style={reusableStyles.bsInput} secureTextEntry={true} placeholder="Password" onChangeText={(text) => this.setState({text})} />
            </View>
            <View style={reusableStyles.elemCntr}>
              <TouchableHighlight underlayColor="#f90000" style={ reusableStyles.bsBtnRed } onPress = { ()=> this.props.navigation.push('Success')}>
                <Text style={ reusableStyles.buttonWhtTxt }>SIGN IN</Text>
              </TouchableHighlight>
            </View>
            <View style={reusableStyles.elemCntr}>
              <TouchableHighlight underlayColor="#f90000" style={ reusableStyles.bsBtnWht } onPress = { ()=> this.props.navigation.push('Signup')} >
                <Text style={ reusableStyles.buttonRedTxt }>SIGN UP</Text>
              </TouchableHighlight>
            </View>
            <View style={reusableStyles.elemCntr}>  
              <TouchableHighlight underlayColor="#fff" style={ reusableStyles.bsBtnLnk } onPress = { ()=> this.props.navigation.push('Error')}>
                <Text style={ reusableStyles.buttonRedTxtSm }>FORGOT PASSWORD</Text>
              </TouchableHighlight>
            </View>
          </View>  
        </View>
      </View>
    );
  }
}
